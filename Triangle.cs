﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace ASE_Assignment
{
    class Triangle : Shape
    {
        Point point1 = new Point(0, 0);
        Point point2 = new Point(0, 0);
        Point point3 = new Point(0, 0);

        public Triangle() : base()
        {

        }

        public Triangle(Color colour, int x, int y, int point1x, int point1y, int point2x, int point2y, int point3x, int point3y) : base(colour, x, y)
        {
            this.point1.X = point1x;
            this.point1.Y = point1y;
            this.point2.X = point2x;
            this.point2.Y = point2y;
            this.point3.X = point3x;
            this.point3.Y = point3y;
        }

        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            this.point1.X = list[2];
            this.point1.Y = list[3];
            this.point2.X = list[4];
            this.point2.Y = list[5];
            this.point3.X = list[6];
            this.point3.Y = list[7];
        }

        public override void draw(Graphics g)
        {
            Point[] trianglePoints = { point1, point2, point3 };
            Pen p = new Pen(Globals.PenWritingColour);
            SolidBrush b = new SolidBrush(colour);
            if (Globals.shapeFill == true)
            {
                g.FillPolygon(b, trianglePoints);
            }
            else if (Globals.shapeDraw == true)
            {
                g.DrawPolygon(p, trianglePoints);
            }
            p.Dispose();
            b.Dispose();
        }


        public override double calcArea()
        {
            return 0;
        }


        public override double calcPerimeter()
        {
            return 0;
        }


    }
}
