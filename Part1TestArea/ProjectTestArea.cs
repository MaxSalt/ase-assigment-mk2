﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_Assignment;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TestArea
{
    [TestClass]
    public class Part1TestArea
    {
        [TestMethod]
        ///<summary>
        ///Basic Syntax check for the 'Forward' command.
        ///Should display error message due to the string being unable to be formatted into a integer.
        ///First test failed as exception handling wasn't properly implemented
        ///Second attempt ran after exception implementation
        ///Second test ran without error
        ///</summary>
        public void CommandParser_BasicTest()
        {

            MainForm x = new MainForm();
            //string provided should produce a 'Format exception' error
            String teststring = "Forward 1o24";
            x.CommandParser(teststring);
        }

        [TestMethod]
        ///<summary>
        ///Basic Multiline command/script test. '\n' used to mimick the input recived if ScriptInput has multiple lines of commands in it.
        ///No errors should occur.
        ///No errors occured. 
        ///</summary>
        public void CommandParser_MultilineTest()
        {
            MainForm x = new MainForm();
            String teststring = "run";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            String multilineString = "forward 200 \nturnleft \nforward 300 \nturnright \nforward 100";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
        }

        [TestMethod]
        ///<summary>
        ///Basic Multiline command/script test. '\n' used to mimick the input recived if ScriptInput has multiple lines of commands in it.
        ///This is to test to see if all commands linked to the switch in the parser are run and recivied correctly in a multiline script
        ///No errors should occur.
        ///No errors occured. 
        ///</summary>
        public void CommandParser_MultilineSwitchTest()
        {
            MainForm x = new MainForm();
            String teststring = "run";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            // openScript and SaveScript are absent as they open up system windows
            String multilineString = "\nturnleft \nturnright \npenup \npendown \nclear";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
        }

        [TestMethod]
        ///<summary>
        ///Basic Multiline command/script test. '\n' used to mimick the input recived if ScriptInput has multiple lines of commands in it.
        ///String supplied has a deliberate error
        ///Test is to see how programs to error mid script
        ///no errors occured. This means that error exception/catching worked however unable to see how this is displayed visually.
        ///will need to run test outside of test enviorment as graphics draw will give better knowledge of where commands stop
        ///</summary>
        public void CommandParser_MultilineErrorTest()
        {
            MainForm x = new MainForm();
            String teststring = "run";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            // string should produce an error
            String multilineString = "forward 200 \nturnleft \nforward three hundred \nturnright \nforward 100";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
        }

        [TestMethod]
        ///<summary>
        ///Basic Syntax check for the 'rectangle'shape.
        ///ShapeParser looks to see if "rect" is present so this should pass the test.
        ///All parameters for the command are valid
        ///Test should complete with no errors
        ///No errors from test
        ///</summary>
        public void ShapeParser_RectTest()
        {
            MainForm x = new MainForm();
            //string provided should produce no errors
            String teststring = "shape rectangle 200 300";
            x.CommandParser(teststring);
        }

        [TestMethod]
        ///<summary>
        ///Basic Syntax check for the 'circle'shape.
        ///ShapeParser looks to see if "rect" is present so this should pass the test.
        ///All parameters for the command are valid
        ///Test should complete with no errors
        ///No errors from test
        ///</summary>
        public void ShapeParser_CircleTest()
        {
            MainForm x = new MainForm();
            //string provided should produce no errors
            String teststring = "shape cricle 50";
            x.CommandParser(teststring);
        }

        [TestMethod]
        ///<summary>
        ///Basic Syntax check for the 'Triangle' shape.
        ///ShapeParser should display an error only 5 values have been given instead of 6
        ///Test should complete with an error
        ///Test 1 produced an System.IndexOutOfRangeException
        ///need to add exceptiton handling for this senario
        ///decided to do check length rather than error exception as this would also check to see iof too many parameters were given
        ///add lenght checks for user input. 
        ///2nd test passed without errors
        ///Have also added length checks for other shapes in light of this testing 
        ///</summary>
        public void ShapeParser_TriangleTest()
        {
            MainForm x = new MainForm();
            //string provided should produce an no error
            String teststring = "shape triangle 200 300 100 100 600";
            x.CommandParser(teststring);
        }

    }

    [TestClass]
    public class Part2TestArea
    {
        [TestMethod]
        ///<summary>
        ///Basic Syntax check for the declaration of variables (Proof of concept)
        ///multinline input is used as variables are likely to be declared in ScriptInput for later implementation into loops
        ///A console output is generated to see if variables have been correctly made in the arraylsit
        ///test data provided should produce no errors 
        ///First test failed. Seems second variable declared either wasn't added to list or overided the first variable
        ///No error on Second test. Error was in data supplied. the space infront of the new line caused the string variable to not be created properly
        ///</summary>
        public void CommandParser_VarNameExceptionTest()
        {
            MainForm x = new MainForm();
            String teststring = "run";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            // string should produce no errors
            String multilineString = "declarevar test! string test";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
            //using variable to get the values created in this test
            Console.WriteLine(x.TextOutput.Text);
        }

        [TestMethod]
        ///<summary>
        ///Basic Syntax check for the declaration of variables (Proof of concept)
        ///multinline input is used as variables are likely to be declared in ScriptInput for later implementation into loops
        ///A console output is generated to see if variables have been correctly made in the arraylsit
        ///test data provided should produce no errors 
        ///First test failed. Seems second variable declared either wasn't added to list or overided the first variable
        ///No error on Second test. Error was in data supplied. the space infront of the new line caused the string variable to not be created properly
        ///</summary>
        public void CommandParser_VariableTest()
        {
            MainForm x = new MainForm();
            String teststring = "run";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            // string should produce no errors
            String multilineString = "declarevar test1 string test\ndeclarevar test2 int 50";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
            //using variable to get the values created in this test
            var Value1 = x.Variables[0];
            var Value2 = x.Variables[1];
            var Value3 = x.Variables[2];
            var Value4 = x.Variables[3];
            //Writeline should show the new variables created and their values in the "Variables" arraylist
            Console.WriteLine(Value1 + " " + Value2 + " " + Value3 + " " + Value4);
        }


        [TestMethod]
        ///<summary>
        ///This test is to see if the '++' functionality of the 'modifyvar' function works properly.
        ///The idea behind this is so that the value of an int variable can increase in order to break loops.
        ///test data provided should produce no errors 
        ///No errors with first test. Test created the extpected output and no errors were thrown.
        ///</summary>
        public void CommandParser_VariableModifyPlus()
        {
            MainForm x = new MainForm();
            String teststring = "run";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            // string should produce no errors
            String multilineString = "declarevar test1 int 50\nmodifyvar test1 int ++";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
            //using variable to get the values created in this test
            var Value1 = x.Variables[0];
            var Value2 = x.Variables[1];
            //Writeline should show the new variables created and their values in the "Variables" arraylist
            Console.WriteLine(x.TextOutput.Text);
            Console.WriteLine(Value1 + " " + Value2);
        }

        [TestMethod]
        ///<summary>
        ///This test is to see if the '--' functionality of the 'modifyvar' function works properly.
        ///The idea behind this is so that the value of an int variable can decrease in order to break loops.
        ///test data provided should produce no errors 
        ///No errors with first test. Test created the extpected output and no errors were thrown.
        ///</summary>
        public void CommandParser_VariableModifyMinus()
        {
            MainForm x = new MainForm();
            String teststring = "run";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            // string should produce no errors
            String multilineString = "declarevar test1 int 50\nmodifyvar test1 int --";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
            //using variable to get the values created in this test
            var Value1 = x.Variables[0];
            var Value2 = x.Variables[1];
            //Writeline should show the new variables created and their values in the "Variables" arraylist
            Console.WriteLine(x.TextOutput.Text);
            Console.WriteLine(Value1 + " " + Value2);
        }

        [TestMethod]
        ///<summary>
        ///This test is to see if the int modify functionality of the 'modifyvar' function works properly.
        ///The idea behind this is so that the value of an int variable can be changes to any other int value in order to break loops or if against.
        ///test data provided should produce no errors 
        ///No errors with first test. Test created the extpected output and no errors were thrown.
        ///</summary>
        public void CommandParser_VariableModifyInt()
        {
            MainForm x = new MainForm();
            String teststring = "run";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            // string should produce no errors
            String multilineString = "declarevar test1 int 50\nmodifyvar test1 int 200";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
            //using variable to get the values created in this test
            var Value1 = x.Variables[0];
            var Value2 = x.Variables[1];
            //Writeline should show the new variables created and their values in the "Variables" arraylist as well as output for the method
            Console.WriteLine(x.TextOutput.Text);
            Console.WriteLine(Value1 + " " + Value2);
        }

        [TestMethod]
        ///<summary>
        ///This test is to see if the string modify functionality of the 'modifyvar' function works properly.
        ///The idea behind this is so that the value of an int variable can be changes to any other int value in order to break loops or if against.
        ///test data provided should produce no errors 
        ///No errors with first test. Test created the extpected output and no errors were thrown.
        ///</summary>
        public void CommandParser_VariableModifyString()
        {
            MainForm x = new MainForm();
            String teststring = "run";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            // string should produce no errors
            String multilineString = "declarevar test1 string hello\nmodifyvar test1 string goodbye";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
            //using variable to get the values created in this test
            var Value1 = x.Variables[0];
            var Value2 = x.Variables[1];
            //Writeline should show the new variables created and their values in the "Variables" arraylist as well as outpout from the method
            Console.WriteLine(x.TextOutput.Text);
            Console.WriteLine(Value1 + " " + Value2);
        }


        [TestMethod]
        ///<summary>
        ///Basic Multiline command/script test using the syntax function. '\n' used to mimick the input recived if ScriptInput has multiple lines of commands in it.
        ///This is to test to see if all commands linked to the switch in the parser are run and recivied correctly in a multiline script
        ///No errors should occur.
        ///No errors occured. 
        ///</summary>
        public void SyntaxParser_MultilineTest()
        {
            MainForm x = new MainForm();
            String teststring = "syntax";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            String multilineString = "forward 200\nturnleft\nforward 300\nturnright\nforward 100";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
            Console.WriteLine(x.TextOutput.Text);
        }

        [TestMethod]
        ///<summary>
        ///Basic Multiline command/script test using the syntax function. '\n' used to mimick the input recived if ScriptInput has multiple lines of commands in it.
        ///This is to test to see if all commands linked to the switch in the parser are run and recivied correctly in a multiline script
        ///No errors should occur.
        ///No errors occured. 
        ///</summary>
        public void SyntaxParser_MultilineSwitchTest()
        {
            MainForm x = new MainForm();
            String teststring = "syntax";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            // openScript and SaveScript are absent as they open up system windows
            String multilineString = "turnleft\nturnright\npenup\npendown\nclear";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
            Console.WriteLine(x.TextOutput.Text);
        }

        [TestMethod]
        ///<summary>
        ///Basic Multiline command/script test using the syntax function. '\n' used to mimick the input recived if ScriptInput has multiple lines of commands in it.
        ///String supplied has a deliberate error
        ///Test is to see how programs to error mid script
        ///no errors occured. This means that error exception/catching worked however unable to see how this is displayed visually.
        ///</summary>
        public void SyntaxParser_MultilineErrorTest()
        {
            MainForm x = new MainForm();
            String teststring = "syntax";
            // \n is used to represent a new line in order to mimic a string recived from ScriptInput
            // string should produce an error
            String multilineString = "forward 200\nturnleft\nforward three hundred\nturnright\nforward 100";
            x.ScriptInput.Text = multilineString;
            x.CommandParser(teststring);
            Console.WriteLine(x.TextOutput.Text);
        }
    }

}
