﻿namespace ASE_Assignment
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DrawArea = new System.Windows.Forms.PictureBox();
            this.TextOutput = new System.Windows.Forms.TextBox();
            this.CommandInput = new System.Windows.Forms.TextBox();
            this.ScriptInput = new System.Windows.Forms.TextBox();
            this.RunButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DrawArea)).BeginInit();
            this.SuspendLayout();
            // 
            // DrawArea
            // 
            this.DrawArea.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DrawArea.Location = new System.Drawing.Point(992, 0);
            this.DrawArea.Name = "DrawArea";
            this.DrawArea.Size = new System.Drawing.Size(1119, 922);
            this.DrawArea.TabIndex = 0;
            this.DrawArea.TabStop = false;
            // 
            // TextOutput
            // 
            this.TextOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextOutput.Location = new System.Drawing.Point(992, 928);
            this.TextOutput.Name = "TextOutput";
            this.TextOutput.ReadOnly = true;
            this.TextOutput.Size = new System.Drawing.Size(903, 35);
            this.TextOutput.TabIndex = 1;
            // 
            // CommandInput
            // 
            this.CommandInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CommandInput.Location = new System.Drawing.Point(-3, 928);
            this.CommandInput.Name = "CommandInput";
            this.CommandInput.Size = new System.Drawing.Size(719, 35);
            this.CommandInput.TabIndex = 2;
            // 
            // ScriptInput
            // 
            this.ScriptInput.Location = new System.Drawing.Point(0, 0);
            this.ScriptInput.Multiline = true;
            this.ScriptInput.Name = "ScriptInput";
            this.ScriptInput.Size = new System.Drawing.Size(796, 922);
            this.ScriptInput.TabIndex = 5;
            // 
            // RunButton
            // 
            this.RunButton.Location = new System.Drawing.Point(612, 987);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(138, 50);
            this.RunButton.TabIndex = 6;
            this.RunButton.Text = "Run";
            this.RunButton.UseVisualStyleBackColor = true;
            this.RunButton.Click += new System.EventHandler(this.RunButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2108, 1326);
            this.Controls.Add(this.RunButton);
            this.Controls.Add(this.ScriptInput);
            this.Controls.Add(this.CommandInput);
            this.Controls.Add(this.TextOutput);
            this.Controls.Add(this.DrawArea);
            this.Name = "MainForm";
            this.Text = "GUI";
            ((System.ComponentModel.ISupportInitialize)(this.DrawArea)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox DrawArea;
        private System.Windows.Forms.TextBox CommandInput;
        private System.Windows.Forms.Button RunButton;
        public System.Windows.Forms.TextBox ScriptInput;
        public System.Windows.Forms.TextBox TextOutput;
    }
}

