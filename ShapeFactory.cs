﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using ASE_Assignment;

class ShapeFactory
	{
	public Shape getShape(String shapeType)
	{
		//trims shaetype and makes it lowercase
		String chosenShape = shapeType.ToLower().Trim();

		if (chosenShape.Equals("circle"))
		{
			//if shape is cricle, program returns a new circle
			return new Circle();
		}
		else if (chosenShape == "rectangle")
		{
			//if rectangle is cricle, program returns a new rectangle
			return new Rectangle();
		}
		else if (chosenShape == "triangle")
		{
			//if shape is triangle, program returns a new triangle
			return new Triangle();
		}
		else
		{
			//if none are correct, an error is thrown
			System.ArgumentException argEx = new System.ArgumentException("Factory error");
			throw argEx;
		
		}
	}







	}
