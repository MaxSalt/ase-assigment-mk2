﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ASE_Assignment
{
    class Circle : Shape
    {
        int radius;

        public Circle() : base() 
        { 
        
        }

        public Circle(Color colour, int x, int y, int radius) : base(colour, x, y)
        {
            this.radius = radius;
        }

        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            this.radius = list[2];
        }


        public override void draw(Graphics g)
        {
            Pen p = new Pen(Globals.PenWritingColour);
            SolidBrush b = new SolidBrush(colour);
            if (Globals.shapeFill == true)
            {
                g.FillEllipse(b, x, y, radius * 2, radius * 2);
            }
            else if (Globals.shapeDraw == true)
            {
                g.DrawEllipse(p, x, y, radius * 2, radius * 2);
            }
            p.Dispose();
            b.Dispose();
        }

        public override double calcArea()
        {
            return Math.PI * (radius ^ 2);
        }

        public override double calcPerimeter()
        {
            return 2 * Math.PI * radius;
        }
    }

}
