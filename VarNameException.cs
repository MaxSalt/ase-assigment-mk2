﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment
{
    class VarNameException : Exception
    {
        public VarNameException()
        {

        }

        public VarNameException(char invalidchar) : base(String.Format("Invalid character present: {0}", invalidchar))
        {

        }
    }
}
