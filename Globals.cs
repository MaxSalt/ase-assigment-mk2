﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ASE_Assignment
{
	class Globals
	{
		// global int declaration
		public static int DefaultxPos = 50;
		public static int DefaultyPos = 50;
		public static int xPos = 50;
		public static int yPos = 50;
		public static int direction = 180;
		public static Color PenWritingColour = Color.Black;
		public static Boolean penWriting = true;
		public static Boolean shapeDraw = true;
		public static Boolean shapeFill = false;
		public static Color shapeFillColour = Color.Blue;
		public static int currentLine = 0;
		public static int ScriptLength = 0;
		public static Boolean LoopActive = false;
		public static Boolean SyntaxMode = false;
		public static Boolean SyntaxError = false;
		public static String SysntaxErrorMessage = "";
	}
}
