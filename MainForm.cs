﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ASE_Assignment;
using System.Collections;

namespace ASE_Assignment
{
	/// <summary>
	/// This is the main class for the program.
	/// This class is also setup as an extenstion of Form
	/// </summary>
	public partial class MainForm : Form
	{
		//A fresh Arraylist called Variables is created on each startup of the program
		public ArrayList Variables = new ArrayList();
		// A new shapefactory is also declared at the beginning.
		ShapeFactory Factory = new ShapeFactory();
		// A Graphic is then created.
		private System.Drawing.Graphics g;
		// A turtule is then created for line drawing.
		Turtle MainDraw;


		/// <summary>
		/// This method is the constructor for the Program
		/// </summary>
		public MainForm()
		{
			// The form is first initinalized.
			InitializeComponent();
			// The graphic G is then updated.
			g = DrawArea.CreateGraphics();
			// Maindraw has been made a fresh turtle.
			MainDraw = new Turtle();
		}
		/// <summary>
		/// This method is used for running a script created by a user within the ScriptInput textbox
		/// </summary>
		public void ScriptRun()
		{
			// variable scriptlength becomes the value of the number of lines present within ScriptInput.
			int scriptlength = ScriptInput.Lines.Length;
			// This value is then updated for the Global value
			Globals.ScriptLength = scriptlength;
			// a while loop is used so that all lines in the script have been read.
			while (Globals.currentLine < scriptlength)
			{
				// The program makes a variable have a string value of the text on the current line it is reading. 
				String CurrentLineValue = ScriptInput.Lines[Globals.currentLine];
				// This is then run to the CommandParserMethod so that it can be processed.
				CommandParser(CurrentLineValue);
				//Global.currentline value is increased by 1 so that on the next itteration of thewhile loop the next line will be read.
				Globals.currentLine++;
			}
			// upon the program being fully run, the Global variables used have their values reset.
			Globals.currentLine = 0; ;
			Globals.ScriptLength = 0;
		}

		/// <summary>
		/// This method is for applying and etsablishing an 'if' condition
		/// </summary>
		/// <param name="lineinput">lineinput is a string passed by the CommandParser that includes the 'declareif' statement declareation.</param>
		/// <example>
		/// The following are valid examples the method expects from lineinput
		/// <br></br>
		/// <code language="cs">
		/// declareif var 'varname' = 10
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// declareif var 'varname' > 10
		/// </code>
		/// <br></br>
		/// </example>
		public void IfScript(string lineinput)
		{
			//whole program is inside a try due to possible faliures when parsing or changing dataypes
			try
			{
				// first a list is constructed for characters to be used to split the value of lineinput.
				char[] delimiterChars = { ' ' };
				// the entire conents of ScriptInput is then split into the new list check.
				String[] check = ScriptInput.Text.Split(delimiterChars);
				// lineinput is then split into the new list ins.
				String[] ins = lineinput.Split(delimiterChars);
				// a new arraylist Ifins is declared.
				ArrayList Ifins = new ArrayList();
				// the contents of the ins list is then added to the Ifins arraylist
				Ifins.AddRange(ins);
				//variable varpos becomes the value of where 'var' is in the Ifins arraylist
				int varpos = Ifins.IndexOf("var");
				varpos++;
				// varname becomes the value of the string stored in location varpos
				var varname = Ifins[varpos];
				// varname pos then becomes the index value of where the variable name is inside of the Variables arraylist.
				var varnamepos = Variables.IndexOf(varname);
				int varvaluepos = varnamepos + 1;
				//variable varvalue is becomes the value of position varvaluepos in Variables arraylist.
				var varValue = Variables[varvaluepos];
				// the following if statements are used to make the variable varValue boolean if it's value is either 'true' or 'false' else it will try to convert it to an integer
				if (varValue.Equals("true"))
				{
					varValue = Convert.ToBoolean(varValue);
				}
				else if (varValue.Equals("false"))
				{
					varValue = Convert.ToBoolean(varValue);
				}
				else
				{
					varValue = Convert.ToInt32(varValue);
				}
				//the same as above is then done to find the IF operator (=, < or >)
				int ifoperatorpos = varpos + 1;
				var ifoperatorvalue = Ifins[ifoperatorpos];
				string operatorstring = ifoperatorvalue.ToString();
				// then the value to compared against is found and becomes the value of variable of comparValue
				int comparValuepos = ifoperatorpos + 1;
				var comparValue = Ifins[comparValuepos];
				TextOutput.Text = ("made to comparvar");
				// the following if statements are used to make the variable comparValue boolean if it's value is either 'true' or 'false' else it will try to convert it to an integer
				if (comparValue.Equals("true"))
				{
					comparValue = Convert.ToBoolean(comparValue);
				}
				else if (comparValue.Equals("false"))
				{
					comparValue = Convert.ToBoolean(comparValue);
				}
				else
				{
					comparValue = Convert.ToInt32(comparValue);
				}
				// Int variables are delcared whose values become the int value of varValue and comparValue
				int intvarvalue = Convert.ToInt32(varValue);
				int intcomparvalue = Convert.ToInt32(comparValue);
				// The following if statement is used to check if the operator is '='
				if (operatorstring.Equals("="))
				{
					//proigram then checks too see if the 2 int values are equal
					if (intvarvalue == intcomparvalue)
					{
						//if they are, the list check is checked to see if 'endif' exists. 
						if (check.Contains("endif"))
						{
							//if 'endif' exists then the program passes 'endif' value on to listpossearcher
							listpossearcher("endif");

						}
						else
						{
							//if 'endif' exists doesn't exist then the program passes 'noif' value on to listpossearcher
							listpossearcher("noif");

						}

					}
					//if the 2 values do not equal then the following happens
					//This is very similair to the method listpossearcher however is was nbot able to make this it's own seperate method due to issues encourntered
					else
					{
						//checks to see if endif is in ScriptInput
						if (ScriptInput.Text.Contains("endif"))
						{
							//various varibales are created in order to keep track of the currentline the search is on as well as what should be the fiorst and last lines.
							int loopendpos = 0;
							int loopcurrentline1 = Globals.currentLine;
							int loopcurrentline2 = loopcurrentline1 - 1;
							int loopstartpos = loopcurrentline1 + 1;
							//while loop is established so that all lines are checked
							while (loopcurrentline1 < Globals.ScriptLength)
							{
								//String variable becomes value of the currentline being checked
								String CurrentLineValue = ScriptInput.Lines[loopcurrentline1];
								//if that string contains endif the following happens
								if (CurrentLineValue.Contains("endif"))
								{
									//variable values change to reflect that the line before is the last line
									loopendpos = loopcurrentline1 - 1;
									//global value for current line is updated
									Globals.currentLine = loopcurrentline1;
									// variable value becomes equal to scriptlength in order to break the loop
									loopcurrentline1 = Globals.ScriptLength;

								}
								else
								{
									// variable value is increaed by 1 so that the next line can be read.
									loopcurrentline1++;
								}
							}

						}
						else
						{
							//otherwise current line value is increased by 1
							Globals.currentLine++;
						}
					}


				}
				//else checks to see if operator is '>'
				else if (operatorstring.Equals(">"))
				{
					//program checks to see if intvarvalue is greater the intcomparvalue
					if (intvarvalue > intcomparvalue)
					{
						//if they are, the list check is checked to see if 'endif' exists. 
						if (check.Contains("endif"))
						{
							//if 'endif' exists then the program passes 'endif' value on to listpossearcher
							listpossearcher("endif");

						}
						else
						{
							//if 'endif' exists doesn't exist then the program passes 'noif' value on to listpossearcher
							listpossearcher("noif");

						}

					}
					//if the 2 values do not equal then the following happens
					//This is very similair to the method listpossearcher however is was nbot able to make this it's own seperate method due to issues encourntered
					else
					{
						//checks to see if endif is in ScriptInput
						if (ScriptInput.Text.Contains("endif"))
						{
							//various varibales are created in order to keep track of the currentline the search is on as well as what should be the fiorst and last lines.
							int loopendpos = 0;
							int loopcurrentline1 = Globals.currentLine;
							int loopcurrentline2 = loopcurrentline1 - 1;
							int loopstartpos = loopcurrentline1 + 1;
							//while loop is established so that all lines are checked
							while (loopcurrentline1 < Globals.ScriptLength)
							{
								//String variable becomes value of the currentline being checked
								String CurrentLineValue = ScriptInput.Lines[loopcurrentline1];
								//if that string contains endif the following happens
								if (CurrentLineValue.Contains("endif"))
								{
									//variable values change to reflect that the line before is the last line
									loopendpos = loopcurrentline1 - 1;
									//global value for current line is updated
									Globals.currentLine = loopcurrentline1;
									// variable value becomes equal to scriptlength in order to break the loop
									loopcurrentline1 = Globals.ScriptLength;

								}
								else
								{
									// variable value is increaed by 1 so that the next line can be read.
									loopcurrentline1++;
								}
							}

						}
						else
						{
							//otherwise current line value is increased by 1
							Globals.currentLine++;
						}
					}


				}
				// else checks to see if operator is '<'
				else if (operatorstring.Equals("<"))
				{
					//program then checks to see if invarvalue is less than intcomparevalue.
					if (intvarvalue < intcomparvalue)
					{
						//if they are, the list check is checked to see if 'endif' exists. 
						if (check.Contains("endif"))
						{
							//if 'endif' exists then the program passes 'endif' value on to listpossearcher
							listpossearcher("endif");

						}
						else
						{
							//if 'endif' exists doesn't exist then the program passes 'noif' value on to listpossearcher
							listpossearcher("noif");

						}

					}
					//if the 2 values do not equal then the following happens
					//This is very similair to the method listpossearcher however is was nbot able to make this it's own seperate method due to issues encourntered
					else
					{
						//checks to see if endif is in ScriptInput
						if (ScriptInput.Text.Contains("endif"))
						{
							//various varibales are created in order to keep track of the currentline the search is on as well as what should be the fiorst and last lines.
							int loopendpos = 0;
							int loopcurrentline1 = Globals.currentLine;
							int loopcurrentline2 = loopcurrentline1 - 1;
							int loopstartpos = loopcurrentline1 + 1;
							//while loop is established so that all lines are checked
							while (loopcurrentline1 < Globals.ScriptLength)
							{
								//String variable becomes value of the currentline being checked
								String CurrentLineValue = ScriptInput.Lines[loopcurrentline1];
								//if that string contains endif the following happens
								if (CurrentLineValue.Contains("endif"))
								{
									//variable values change to reflect that the line before is the last line
									loopendpos = loopcurrentline1 - 1;
									//global value for current line is updated
									Globals.currentLine = loopcurrentline1;
									// variable value becomes equal to scriptlength in order to break the loop
									loopcurrentline1 = Globals.ScriptLength;

								}
								else
								{
									// variable value is increaed by 1 so that the next line can be read.
									loopcurrentline1++;
								}
							}

						}
						else
						{
							//otherwise current line value is increased by 1
							Globals.currentLine++;
						}
					}


				}
				//if no valid operator is given then the following happens
				else
				{
					//text output ot the user is operaot is invalid
					TextOutput.Text = "Decleration failed due to invalid values";
					//outputs for syntax checks are updated to refelct that the operator was incorrect.
					if (Globals.SyntaxMode == true)
					{
						Globals.SyntaxError = true;
						Globals.SysntaxErrorMessage = "Invalid operator used";
					}
				}
			}
			catch
			{
				//text output to the user is decleration failed.
				TextOutput.Text = "Decleration of IF failed due to invalid values";
				//outputs for syntax checks are updated to refelct that decleration failed.
				if (Globals.SyntaxMode == true)
				{
					Globals.SyntaxError = true;
					Globals.SysntaxErrorMessage = "Decleration of IF failed due to invalid values";
				}
			}

		}


		/// <summary>
		/// This method is for applying and etsablishing a 'loop' condition.
		/// NOTE: As of submission this method does not work fully however it is being left in the program for possible credit.
		/// </summary>
		/// <param name="lineinput">lineinput is a string passed by the CommandParser that includes the 'declareloop' statement declareation.</param>
		/// <example>
		/// The following are valid examples the method expects from lineinput
		/// <br></br>
		/// <code language="cs">
		/// declareloop var 'varname' = 10
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// declareloop var 'varname' > 10
		/// </code>
		/// <br></br>
		/// </example>
		public void WhileLoop(string lineinput)
		{
			// first a list is constructed for characters to be used to split the value of lineinput.
			char[] delimiterChars = { ' ' };
			// the entire conents of ScriptInput is then split into the new list check.
			String[] check = ScriptInput.Text.Split(delimiterChars);
			// lineinput is then split into the new list ins.
			String[] ins = lineinput.Split(delimiterChars);
			// a new arraylist Ifins is declared.
			ArrayList Ifins = new ArrayList();
			// the contents of the ins list is then added to the Ifins arraylist
			Ifins.AddRange(ins);
			// variable varpos becomes the value of where 'var' is in the Ifins arraylist
			int varpos = Ifins.IndexOf("var");
			varpos++;
			// varname becomes the value of the string stored in location varpos
			var varname = Ifins[varpos];
			// varname pos then becomes the index value of where the variable name is inside of the Variables arraylist.
			var varnamepos = Variables.IndexOf(varname);
			int varvaluepos = varnamepos + 1;
			//variable varvalue is becomes the value of position varvaluepos in Variables arraylist.
			var varValue = Variables[varvaluepos];
			// the following if statements are used to make the variable varValue boolean if it's value is either 'true' or 'false' else it will try to convert it to an integer
			if (varValue.Equals("true"))
			{
				varValue = Convert.ToBoolean(varValue);
			}
			else if (varValue.Equals("false"))
			{
				varValue = Convert.ToBoolean(varValue);
			}
			else
			{
				varValue = Convert.ToInt32(varValue);
			}
			//the same as above is then done to find the IF operator (=, < or >)
			int ifoperatorpos = varpos + 1;
			var ifoperatorvalue = Ifins[ifoperatorpos];
			string operatorstring = ifoperatorvalue.ToString();
			// then the value to compared against is found and becomes the value of variable of comparValue
			int comparValuepos = ifoperatorpos + 1;
			var comparValue = Ifins[comparValuepos];
			TextOutput.Text = ("made to comparvar");
			// the following if statements are used to make the variable comparValue boolean if it's value is either 'true' or 'false' else it will try to convert it to an integer
			if (comparValue.Equals("true"))
			{
				comparValue = Convert.ToBoolean(comparValue);
			}
			else if (comparValue.Equals("false"))
			{
				comparValue = Convert.ToBoolean(comparValue);
			}
			else
			{
				comparValue = Convert.ToInt32(comparValue);
			}
			// Int variables are delcared whose values become the int value of varValue and comparValue
			int intvarvalue = Convert.ToInt32(varValue);
			int intcomparvalue = Convert.ToInt32(comparValue);
			// The following if statement is used to check if the operator is '='
			if (operatorstring.Equals("="))
			{
				//program then checks too see if the 2 int values are equal
				if (intvarvalue == intcomparvalue)
				{
					//if they are, the list check is checked to see if 'endloop' exists. 
					if (check.Contains("endloop"))
					{
						//if 'endif' exists then the program passes 'endloop' value on to listpossearcher
						listpossearcher("endloop");

					}
					else
					{
						//if 'endloop' exists doesn't exist then the program passes 'noend' value on to listpossearcher
						listpossearcher("noend");

					}

				}
				//if the 2 values do not equal then the following happens
				//This is very similair to the method listpossearcher however is was nbot able to make this it's own seperate method due to issues encourntered
				else
				{
					//checks to see if endloop is in ScriptInput
					if (ScriptInput.Text.Contains("endloop"))
					{
						//various varibales are created in order to keep track of the currentline the search is on as well as what should be the fiorst and last lines.
						int loopendpos = 0;
						int loopcurrentline1 = Globals.currentLine;
						int loopcurrentline2 = loopcurrentline1 - 1;
						int loopstartpos = loopcurrentline1 + 1;
						//while loop is established so that all lines are checked
						while (loopcurrentline1 < Globals.ScriptLength)
						{
							//String variable becomes value of the currentline being checked
							String CurrentLineValue = ScriptInput.Lines[loopcurrentline1];
							//if that string contains endloop the following happens
							if (CurrentLineValue.Contains("endloop"))
							{
								//variable values change to reflect that the line before is the last line
								loopendpos = loopcurrentline1 - 1;
								//global value for current line is updated
								Globals.currentLine = loopcurrentline1;
								// variable value becomes equal to scriptlength in order to break the loop
								loopcurrentline1 = Globals.ScriptLength;

							}
							else
							{
								// variable value is increaed by 1 so that the next line can be read.
								loopcurrentline1++;
							}
						}

					}
					else
					{
						//otherwise current line value is increased by 1
						Globals.currentLine++;
					}
				}


			}
			//else checks to see if operator is '>'
			else if (operatorstring.Equals(">"))
			{
				//program checks to see if intvarvalue is greater the intcomparvalue
				if (intvarvalue > intcomparvalue)
				{
					//if they are, the list check is checked to see if 'endloop' exists. 
					if (check.Contains("endloop"))
					{
						//if 'endloop' exists then the program passes 'endloop' value on to listpossearcher
						listpossearcher("endloop");

					}
					else
					{
						//if 'endloop' exists doesn't exist then the program passes 'noend' value on to listpossearcher
						listpossearcher("noend");

					}

				}
				//if the 2 values do not equal then the following happens
				//This is very similair to the method listpossearcher however is was nbot able to make this it's own seperate method due to issues encourntered
				else
				{
					//checks to see if endloop is in ScriptInput
					if (ScriptInput.Text.Contains("endloop"))
					{
						//various varibales are created in order to keep track of the currentline the search is on as well as what should be the fiorst and last lines.
						int loopendpos = 0;
						int loopcurrentline1 = Globals.currentLine;
						int loopcurrentline2 = loopcurrentline1 - 1;
						int loopstartpos = loopcurrentline1 + 1;
						//while loop is established so that all lines are checked
						while (loopcurrentline1 < Globals.ScriptLength)
						{
							//String variable becomes value of the currentline being checked
							String CurrentLineValue = ScriptInput.Lines[loopcurrentline1];
							//if that string contains endloop the following happens
							if (CurrentLineValue.Contains("endloop"))
							{
								//variable values change to reflect that the line before is the last line
								loopendpos = loopcurrentline1 - 1;
								//global value for current line is updated
								Globals.currentLine = loopcurrentline1;
								// variable value becomes equal to scriptlength in order to break the loop
								loopcurrentline1 = Globals.ScriptLength;

							}
							else
							{
								// variable value is increaed by 1 so that the next line can be read.
								loopcurrentline1++;
							}
						}

					}
					else
					{
						//otherwise current line value is increased by 1
						Globals.currentLine++;
					}
				}


			}
			// else checks to see if operator is '<'
			else if (operatorstring.Equals("<"))
			{
				//program then checks to see if invarvalue is less than intcomparevalue.
				if (intvarvalue < intcomparvalue)
				{
					//if they are, the list check is checked to see if 'endloop' exists. 
					if (check.Contains("endloop"))
					{
						//if 'endif' exists then the program passes 'endif' value on to listpossearcher
						listpossearcher("endloop");

					}
					else
					{
						//if 'endloop' exists doesn't exist then the program passes 'noend' value on to listpossearcher
						listpossearcher("noend");

					}

				}
				//if the 2 values do not equal then the following happens
				//This is very similair to the method listpossearcher however is was nbot able to make this it's own seperate method due to issues encourntered
				else
				{
					//checks to see if endloop is in ScriptInput
					if (ScriptInput.Text.Contains("endloop"))
					{
						//various varibales are created in order to keep track of the currentline the search is on as well as what should be the fiorst and last lines.
						int loopendpos = 0;
						int loopcurrentline1 = Globals.currentLine;
						int loopcurrentline2 = loopcurrentline1 - 1;
						int loopstartpos = loopcurrentline1 + 1;
						//while loop is established so that all lines are checked
						while (loopcurrentline1 < Globals.ScriptLength)
						{
							//String variable becomes value of the currentline being checked
							String CurrentLineValue = ScriptInput.Lines[loopcurrentline1];
							//if that string contains endloop the following happens
							if (CurrentLineValue.Contains("endloop"))
							{
								//variable values change to reflect that the line before is the last line
								loopendpos = loopcurrentline1 - 1;
								//global value for current line is updated
								Globals.currentLine = loopcurrentline1;
								// variable value becomes equal to scriptlength in order to break the loop
								loopcurrentline1 = Globals.ScriptLength;

							}
							else
							{
								// variable value is increaed by 1 so that the next line can be read.
								loopcurrentline1++;
							}
						}

					}
					else
					{
						//otherwise current line value is increased by 1
						Globals.currentLine++;
					}
				}


			}
			//if no valid operator is given then the following happens
			else
			{
				//outputs for syuntax checks are updated to refelct that the operator was incorrect.
				if (Globals.SyntaxMode == true)
				{
					Globals.SyntaxError = true;
					Globals.SysntaxErrorMessage = "Invalid operator used";
				}
			}

		}

		/// <summary>
		/// This method is for finding the finding the start end and end lines for 'if' and 'loop' statements
		/// NOTE: as of submission this does not interact with values passed on by the WhileLoop method due to issues with that methods production
		/// </summary>
		/// <param name="endingvalue">endingvalue is a string value which the program will check against in order to set certain values</param>
		/// <example>
		/// The following are valid examples the method expects from lineinput
		/// <br></br>
		/// <code language="cs">
		/// listpossearcher("endif")
		/// </code>
		/// </example>
		public void listpossearcher(string endingvalue)
		{
			// 2 boolean variables are created that will be used to check against
			Boolean validcheck = false;
			Boolean noIf = false;
			//if is used to see if endingvalue is either 'endif' or 'noif' and adjusts the corrisponding variable
			if (endingvalue.Equals("endif"))
			{
				validcheck = true;
			}
			else if (endingvalue.Equals("noif"))
			{
				noIf = true;
			}
			//if endingvalue is 'endif' then following if statement will process
			if (validcheck == true)
			{
				//Int variables declared for start position, end position and for use in this area of code.
				int loopstartpos = Globals.currentLine - 1;
				int loopendpos = 0;
				int loopcurrentline1 = Globals.currentLine - 1;
				// a while loop is establiushed so all lines can be checked if needed
				while (loopcurrentline1 < Globals.ScriptLength)
				{
					//String variable becomes value of the current line being looked at
					String CurrentLineValue = ScriptInput.Lines[loopcurrentline1];
					//checks to see if 'enfif' is in string value
					if (CurrentLineValue.Contains("endif"))
					{
						//variable values are adjusted
						loopendpos = loopcurrentline1 - 1;
						Globals.currentLine = loopcurrentline1;
						//LoopScript is then run using the value of the starting position and loopcurrentline1
						LoopScript(loopstartpos, loopcurrentline1);
						//loopcurrentline1 becoimes the value of Globals.ScriptLength in order to break the while loop
						loopcurrentline1 = Globals.ScriptLength;

					}
					else
					{
						loopcurrentline1++;
					}
				}
			}
			//the following is run if noIf is true instead
			else if (noIf == true)
			{
				//declare int variables and their values
				int loopstartpos2 = Globals.currentLine;
				int loopendpos2 = Globals.currentLine;
				//increase int variables values by 1
				loopstartpos2++;
				loopendpos2++;
				//pass the 2 int variables onto LoopScript
				LoopScript(loopstartpos2, loopendpos2);
				Globals.currentLine++;
			}
			else
			{
				//declare int variables and their values
				int loopstartpos2 = Globals.currentLine;
				int loopendpos2 = Globals.currentLine;
				//increase int variables values by 1
				loopstartpos2++;
				loopendpos2++;
				//pass the 2 int variables onto LoopScript
				LoopScript(loopstartpos2, loopendpos2);
				Globals.currentLine++;
			}
		}

		/// <summary>
		/// This method is used to read the specific lines for if and loop statements
		/// </summary>
		/// <param name="startpos">The integer value for the first line in the loop</param>
		/// <param name="endpos">The integer value for the last line in the loop</param>
		/// <example>
		/// The following are valid examples the method expects from lineinput
		/// <br></br>
		/// <code language="cs">
		/// LoopScript(Startline, Endline)
		/// </code>
		/// </example>
		public void LoopScript(int startpos, int endpos)
		{
			//int variables values become that of startpos and endpos which have been passed to this method.
			int currentline = startpos;
			int endline = endpos;
			//a while loop is started to run the lines declared. 
			while (currentline < endline)
			{
				//String variable's value becomes that of the current line
				String CurrentLineValue = ScriptInput.Lines[currentline];
				//that string is then passed into CommandParser to be processed
				CommandParser(CurrentLineValue);
				currentline++;
			}
		}
		/// <summary>
		/// This method is used to open a pre-made script from a text file (.txt) using Windows OpenFileDialog window
		/// </summary>
		public void OpenScript()
		{
			//ScriptInput is first cleared
			ScriptInput.Clear();
			//Program creates a new OpenFileDialog
			OpenFileDialog openFile = new OpenFileDialog();
			//A filter is defined as the program is looking for only text files
			openFile.Filter = "Text Files (.txt)| *.txt";
			//string variable declared but left empty
			string line = "";
			//when user presses ok to open in the Windows OpenFileDialog window
			if (openFile.ShowDialog() == DialogResult.OK)
			{
				//new StreamReader is Declared
				StreamReader openRead = new StreamReader(openFile.FileName);
				//while loop started
				while (line != null)
				{
					line = openRead.ReadLine();
					if (line != null)
					{
						ScriptInput.Text += line + Environment.NewLine;
					}
				}
				//once file has been read the StreamReader is closed
				openRead.Close();
			}
		}

		/// <summary>
		/// This method is used to save a pre-made script from a text file (.txt) using Windows SaveFileDialog window
		/// </summary>
		public void SaveScript()

		{
			//Program creates a new SaveFileDialog
			SaveFileDialog saveFile = new SaveFileDialog();
			//filter ios made as program is only looking to make text files
			saveFile.Filter = "Text Files (.txt)| *.txt";
			//when user presses ok to save in the Windows OpenFileDialog window
			if (saveFile.ShowDialog() == DialogResult.OK)
			{
				//filepath becomes that chosen by the user
				var filePath = saveFile.FileName;
				//new StreamWriter is created
				StreamWriter fileWriter = new StreamWriter(filePath);
				//StreamWriter then writes the contnet of ScriptInput into the file
				fileWriter.Write(ScriptInput.Text);
				//The StreamWriter is then closed
				fileWriter.Close();
			}
		}

		/// <summary>
		/// This Method is used to Draw Shapes
		/// </summary>
		/// <param name="LineInput">LineInput is a string value which contains the shape decleration</param>
		/// <example>
		/// The following are valid examples the method expects from LineInput
		/// <br></br>
		/// <code language="cs">
		/// shape circle 50
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// shape circle var 'varname'
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// shape rect 100 200
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// shape rect var 'varname' 200
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// shape rect 100 var 'varname'
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// shape rect var 'varname' var 'varname'
		/// </code>
		/// <code language="cs">
		/// shape triangle 100 100 150 300 450 120
		/// </code>
		/// </example>
		public void ShapeParser(String LineInput)
		{
			//string value becomes that of LineInput
			String CommandValue = LineInput;
			//character lsit made to split against
			char[] delimiterChars = { ' ' };
			//makes CommandValue lowercase
			CommandValue = CommandValue.ToLower();
			// checks to see if CommandValue contains shape
			if (CommandValue.Contains("shape"))
			{
				// splits CommandValue in list ins
				String[] ins = CommandValue.Split(delimiterChars);
				// int variable becomes the length of ins
				int testlength = ins.Length;
				// program gets the shape asked for
				String shape = ins[1].ToString();
				// if the shape asked for a is a circle the following is done
				if (shape.Contains("circle"))
				{
					// checks to see if the length of ins is less than or equal to 4
					if (ins.Length <= 4)
					{
						try
						{
							//if lenght is 3
							if (ins.Length == 3)
							{
								// circleraidus becomes int value of the value in postion 2 of ins
								int circleRadius = Int16.Parse(ins[2]);
								// new shape is made
								Shape s;
								// shape goes to shapefactory to get circle
								s = Factory.getShape("circle");
								// the values for the circle are then set
								s.set(Globals.shapeFillColour, Globals.xPos, Globals.yPos, circleRadius);
								// circle is drawn
								s.draw(g);
							}
							//if 'var' is present in postion 
							else if (ins[2].Equals("var"))
							{
								//program grabs variable name and value from Variables arraylist
								int varnamepos = Variables.IndexOf(ins[3]);
								int varvaluepos = varnamepos + 1;
								var variablevalue = Variables[varvaluepos];
								String VariablevalueString = variablevalue.ToString();
								//program then tries to make variablevaluestring an integer
								int Varcurrentvalue = Int16.Parse(VariablevalueString);
								// new shape is declared
								Shape s;
								// shape goes to shape factory to get circle
								s = Factory.getShape("circle");
								// the values for circle are then set
								s.set(Globals.shapeFillColour, Globals.xPos, Globals.yPos, Varcurrentvalue);
								// circle is drawn
								s.draw(g);
							}
							else
							{
								// output to the user that there are invalid pararmeters
								TextOutput.Text = "Invalid Parameters";
								// if in syntax mode
								if (Globals.SyntaxMode == true)
								{
									// syntax is updated and error message stated
									Globals.SyntaxError = true;
									Globals.SysntaxErrorMessage = "Invalid Parameters given";
								}
							}
						}
						catch (System.FormatException e)
						{
							// outputs to user that there are invalid parameters
							TextOutput.Text = "Invalid Parameters";
							// if in syntax mode
							if (Globals.SyntaxMode == true)
							{
								// syntax is updated and error message stated
								Globals.SyntaxError = true;
								Globals.SysntaxErrorMessage = "Non integer value given";
							}
						}
					}
					else
					{
						TextOutput.Text = "Invalid Number of Parameters";
						// if in syntax mode
						if (Globals.SyntaxMode == true)
						{
							// syntax is updated and error message stated
							Globals.SyntaxError = true;
							Globals.SysntaxErrorMessage = "Invalid number of Parameters given";
						}
					}
				}
				// else checkif shape is 'rect'
				else if (shape.Contains("rect"))
				{
					//checks to see if length is equal or less than 6
					if (ins.Length <= 6)
					{
						try
						{
							//checks to see if length is 4
							if (ins.Length == 4)
							{
								// width and height are declared
								int rectWidth = Int16.Parse(ins[2]);
								int rectHeight = Int16.Parse(ins[3]);
								// new shape is made
								Shape s;
								// shape goes to shape factory to get rectangle
								s = Factory.getShape("rectangle");
								// shape values are set
								s.set(Globals.shapeFillColour, Globals.xPos, Globals.yPos, rectWidth, rectHeight);
								// rectangle is drawn
								s.draw(g);
							}
							// else check if lenght is 5
							else if (ins.Length == 5)
							{
								// checks to see the location of 'var'
								if (ins[2].Equals("var"))
								{
									//program gets variable value
									int varnamepos = Variables.IndexOf(ins[3]);
									int varvaluepos = varnamepos + 1;
									var variablevalue = Variables[varvaluepos];
									String VariablevalueString = variablevalue.ToString();
									//variable value is then made into an integer
									int Varcurrentvalue = Int16.Parse(VariablevalueString);
									// variable values are set
									int rectWidth = Varcurrentvalue;
									int rectHeight = Int16.Parse(ins[4]);
									// new shape is made
									Shape s;
									// shape goes to factory to get rectangle
									s = Factory.getShape("rectangle");
									// shape values are then set
									s.set(Globals.shapeFillColour, Globals.xPos, Globals.yPos, rectWidth, rectHeight);
									// rectangle is drawn
									s.draw(g);
								}
								// checks to see the location of 'var'
								else if (ins[3].Equals("var"))
								{
									//program gets variable value
									int varnamepos = Variables.IndexOf(ins[4]);
									int varvaluepos = varnamepos + 1;
									var variablevalue = Variables[varvaluepos];
									String VariablevalueString = variablevalue.ToString();
									//variable value is then made into an integer
									int Varcurrentvalue = Int16.Parse(VariablevalueString);
									// variable values are set
									int rectWidth = Int16.Parse(ins[2]);
									int rectHeight = Varcurrentvalue;
									// new shape is made
									Shape s;
									// shape goes to factory and gets rectangle
									s = Factory.getShape("rectangle");
									// shape values are set
									s.set(Globals.shapeFillColour, Globals.xPos, Globals.yPos, rectWidth, rectHeight);
									// rectangle is drawn
									s.draw(g);
								}
								else
								{
									//Output to user that there are invalid paramters
									TextOutput.Text = "Invalid Parameters";
									// if in syntax mode
									if (Globals.SyntaxMode == true)
									{
										//syntax values updated and erro message is set
										Globals.SyntaxError = true;
										Globals.SysntaxErrorMessage = "Var delcared incorrectly";
									}
								}
							}
							// else checks if lenght is 6
							else if (ins.Length == 6)
							{
								//checks to set if first expected var is in the right place
								if (ins[2].Equals("var"))
								{
									//checks to set if second expected var is in the right place
									if (ins[4].Equals("var"))
									{
										//program gets the first variable value
										int varnamepos = Variables.IndexOf(ins[3]);
										int varvaluepos = varnamepos + 1;
										var variablevalue = Variables[varvaluepos];
										String VariablevalueString = variablevalue.ToString();
										//first variable value is made into an int
										int Varcurrentvalue = Int16.Parse(VariablevalueString);
										//program gets the second variable value
										int varnamepos2 = Variables.IndexOf(ins[5]);
										int varvaluepos2 = varnamepos2 + 1;
										var variablevalue2 = Variables[varvaluepos2];
										String VariablevalueString2 = variablevalue2.ToString();
										//second variable value is made an integer
										int Varcurrentvalue2 = Int16.Parse(VariablevalueString2);
										//variable values are set
										int rectWidth = Varcurrentvalue;
										int rectHeight = Varcurrentvalue2;
										// new shape is made
										Shape s;
										// shape goes to factopry to get rectangle
										s = Factory.getShape("rectangle");
										// shape values are set
										s.set(Globals.shapeFillColour, Globals.xPos, Globals.yPos, rectWidth, rectHeight);
										// rectangle is drawn
										s.draw(g);
									}
									else
									{
										// output to user that invalid paramters
										TextOutput.Text = "Invalid Parameters";
										//if in syntax mode
										if (Globals.SyntaxMode == true)
										{
											//syntax values updated and error message is set
											Globals.SyntaxError = true;
											Globals.SysntaxErrorMessage = "Height Var value is invalid";
										}
									}
								}
								else
								{
									// output to user that invalid paramters
									TextOutput.Text = "Invalid Parameters";
									//if in syntax mode
									if (Globals.SyntaxMode == true)
									{
										//syntax values updated and error message is set
										Globals.SyntaxError = true;
										Globals.SysntaxErrorMessage = "Width var value is invalid";
									}
								}
							}
							else
							{
								// output to user that invalid paramters
								TextOutput.Text = "Invalid Parameters";
								//if in syntax mode
								if (Globals.SyntaxMode == true)
								{
									//syntax values updated and error message is set
									Globals.SyntaxError = true;
									Globals.SysntaxErrorMessage = "Invalid number of Parameters given";
								}
							}
						}
						catch (System.FormatException e)
						{
							// output to user that invalid paramters
							TextOutput.Text = "Invalid Parameters";
							//if in syntax mode
							if (Globals.SyntaxMode == true)
							{
								//syntax values updated and error message is set
								Globals.SyntaxError = true;
								Globals.SysntaxErrorMessage = "Invalid datatype given";
							}
						}
					}
					else
					{
						// output to user that invalid number of paramters
						TextOutput.Text = "Invalid Number of Parameters";
						//if in syntax mode
						if (Globals.SyntaxMode == true)
						{
							//syntax values updated and error message is set
							Globals.SyntaxError = true;
							Globals.SysntaxErrorMessage = "Invalid number of Parameters given";
						}
					}
				}
				//else checks if shape is triangle
				else if (shape.Contains("triangle"))
				{
					// checks to see if lenght is 8
					if (ins.Length == 8)
					{
						try
						{
							//points are set based on expected positions in the ins list
							int point1x = Int16.Parse(ins[2]);
							int point1y = Int16.Parse(ins[3]);
							int point2x = Int16.Parse(ins[4]);
							int point2y = Int16.Parse(ins[5]);
							int point3x = Int16.Parse(ins[6]);
							int point3y = Int16.Parse(ins[7]);
							// new shape is made
							Shape s;
							// shape goes to factory to get triangle
							s = Factory.getShape("triangle");
							// shape values set
							s.set(Globals.shapeFillColour, Globals.xPos, Globals.yPos, point1x, point1y, point2x, point2y, point3x, point3y);
							//triangle is drawn
							s.draw(g);
						}
						catch (System.FormatException e)
						{
							// output to user that invalid number of paramters
							TextOutput.Text = "Invalid Parameters";
							// if in syntax mode
							if (Globals.SyntaxMode == true)
							{
								//syntax values updated and error message is set
								Globals.SyntaxError = true;
								Globals.SysntaxErrorMessage = "Invalid datatype given";
							}
						}
					}
					else
					{
						// output to user that invalid number of paramters
						TextOutput.Text = "Invalid Number of Parameters";
						// if in syntax mode
						if (Globals.SyntaxMode == true)
						{
							//syntax values updated and error message is set
							Globals.SyntaxError = true;
							Globals.SysntaxErrorMessage = "Invalid number of Parameters given";
						}
					}
				}
				else
				{
					// output to user that invalid shape
					TextOutput.Text = "Invalid Shape";
					//if in syntax mode
					if (Globals.SyntaxMode == true)
					{
						//syntax values updated and error message is set
						Globals.SyntaxError = true;
						Globals.SysntaxErrorMessage = "Invalid shape";
					}
				}
			}
			else
			{
				// output to user that invalid command
				TextOutput.Text = "Invalid Command";
				//if in syntax mode
				if (Globals.SyntaxMode == true)
				{
					//syntax values updated and error message is set
					Globals.SyntaxError = true;
					Globals.SysntaxErrorMessage = "Invalid command";
				}
			}
		}

		/// <summary>
		/// This method is used to check the syntax of a script
		/// </summary>
		public void SyntaxParser()
		{
			//Variablers values are changed to let the program know it is in syntax mode
			Globals.SyntaxMode = true;
			Globals.SyntaxError = false;
			Globals.SysntaxErrorMessage = "";
			// variable scriptlength becomes the value of the number of lines present within ScriptInput.
			int scriptlength = ScriptInput.Lines.Length;
			// This value is then updated for the Global value
			Globals.ScriptLength = scriptlength;
			// a while loop is used so that all lines in the script have been read.
			while (Globals.currentLine < scriptlength)
			{
				//String variable becomes the value of the current line being read
				String CurrentLineValue = ScriptInput.Lines[Globals.currentLine];
				//String value id then run through the CommandParser
				CommandParser(CurrentLineValue);
				//The picturebox/canvas is then cleared
				MainDraw.Clear(g);
				//if an error has occured during the syntax then the folloiwng happens
				if (Globals.SyntaxError == true)
				{
					//if an error has occured the loop breaks stopping the program at this line
					break;
				}
				Globals.currentLine++;
			}
			//program checks to see if Globals.SyntaxError == true
			if (Globals.SyntaxError == true)
			{
				//int vcariable value becomes that of the current line being read plus 1 so the user can understand it
				int outputline = Globals.currentLine + 1;
				//an error is provided to the user stating the line the error is on and the errormessage
				TextOutput.Text = "Syntax error on line " + outputline + ".Error Message: " + Globals.SysntaxErrorMessage;
			}
			//if there are no isses then the following is run
			else
			{
				TextOutput.Text = "No syntax errors found";

			}
			//after the syntax check is run all the varaibles used are reset as well as the Variables Arraylist and the picture box cleared and reset
			MainDraw.Clear(g);
			Globals.SyntaxMode = true;
			Globals.SyntaxError = false;
			Globals.SysntaxErrorMessage = "";
			Variables.Clear();
			MainDraw.Reset();
		}
		/// <summary>
		/// This is the main parser of the program.
		/// This method is responsible for processing user input and directing the program to the correct area depending on inputs
		/// </summary>
		/// <param name="LineInput">String LineInput is the currentline being run by the program</param>
		/// <example>
		/// The following are valid examples the method expects from lineinput
		/// <br></br>
		/// <code language="cs">
		/// declareif var 'varname' = 10
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// declareloop var 'varname' = 10
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// declarevar 'varname' int 30
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// declarevar 'varname' string hello
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// modifyvar 'varname' int ++
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// modifyvar 'varname' string goodbye
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// shape rect var 'varname' 200
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// shape rect var 'varname' 200
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// shape rect 100 var 'varname'
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// shape triangle 100 100 150 300 450 120
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// reset
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// clear
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// forward 100
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// backwards 200
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// tunrleft
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// turnright
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// pencolour
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// fillcolour
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// fillobjects
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// drawto 100 100
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// moveto 100 100
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// savescript
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// openscript
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// run
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// syntax
		/// </code>
		/// <br></br>
		/// </example>
		public void CommandParser(string LineInput)
		{
			// Textoutput is clear
			TextOutput.Clear();
			// string variables value becomes that of LineInput
			String CommandValue = LineInput;
			// string is made lowercase
			CommandValue = CommandValue.ToLower();
			// CommandInput is cleared
			CommandInput.Clear();
			// character list is made
			char[] delimiterChars = { ' ' };
			//The following are various if statements used to check the user input and move the program to the right area accordingly
			if (CommandValue.Contains("shape"))
			{
				//if CommandValue contains shape, ShapeParser is run with CommandValue being sent
				ShapeParser(CommandValue);
			}
			else if (CommandValue.Contains("declareif"))
			{
				//if CommandValue contains declareif, IfScript is run with CommandValue being sent
				IfScript(CommandValue);
			}
			else if (CommandValue.Contains("declareloop"))
			{
				//if CommandValue contains declareloop, WhileLoop is run with CommandValue being sent
				WhileLoop(CommandValue);
			}
			else if (CommandValue.Contains("syntax"))
			{
				//if CommandValue contains syntax, SyntaxParser is run
				SyntaxParser();
			}
			else if (CommandValue.Contains("moveto"))
			{
				//list ins made by splitting CommandValue
				String[] ins = CommandValue.Split(delimiterChars);
				// checks to see if ins.length == 3
				if (ins.Length == 3)
				{
					//X and y variables made
					String x = ins[1];
					String y = ins[2];
					//x and y values made into ints
					int xPos = Int16.Parse(x);
					int yPos = Int16.Parse(y);
					//Movepen is run using those x and y values
					MainDraw.MovePen(xPos, yPos);
				}
				else
				{
					//suitable output to user
					TextOutput.Text = "Invalid Command";
					// if in syntax mode
					if (Globals.SyntaxMode == true)
					{
						// syntax is updated and error message stated
						Globals.SyntaxError = true;
						Globals.SysntaxErrorMessage = "Invalid command";
					}

				}
			}
			else if (CommandValue.Contains("pencolour"))
			{
				// new ColourDialog is made
				ColorDialog penDialog = new ColorDialog();
				//when ColourDialog is closed
				if (penDialog.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
				{
					//pencolour is updated
					Globals.PenWritingColour = penDialog.Color;
				}
				//dialog is disposed
				penDialog.Dispose();
			}
			else if (CommandValue.Contains("fillcolour"))
			{
				// new ColourDialog is made
				ColorDialog fillDialog = new ColorDialog();
				//when ColourDialog is closed
				if (fillDialog.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
				{
					//fillcolour is updated
					Globals.shapeFillColour = fillDialog.Color;
				}
				// dialog is disposed
				fillDialog.Dispose();
			}
			else if (CommandValue.Contains("fillobjects"))
			{
				//list ins made by splitting CommandValue
				String[] ins = CommandValue.Split(delimiterChars);
				//checks length of ins list
				if (ins.Length == 2)
				{
					//string value should become either true or false
					String w = ins[1];
					try
					{
						//variables becomes boolean of string integer
						Boolean Decision = Boolean.Parse(w);
						//pases boolean variables to FillObjects
						MainDraw.FillObjects(Decision);
					}
					catch (System.FormatException e)
					{
						//suitable output to user
						TextOutput.Text = "Parameter value given is not boolean";
						// if in syntax mode
						if (Globals.SyntaxMode == true)
						{
							// syntax is updated and error message stated
							Globals.SyntaxError = true;
							Globals.SysntaxErrorMessage = "Parameter value given is not boolean";
						}
					}

				}
				else
				{
					//suitable output to user
					TextOutput.Text = "Invalid Command";
					// if in syntax mode
					if (Globals.SyntaxMode == true)
					{
						// syntax is updated and error message stated
						Globals.SyntaxError = true;
						Globals.SysntaxErrorMessage = "Invalid number of Parameters given";
					}
				}
			}
			else if (CommandValue.Contains("drawto"))
			{
				//list ins made by splitting CommandValue
				String[] ins = CommandValue.Split(delimiterChars);
				//checks length of ins
				if (ins.Length == 3)
				{
					try
					{
						//program tries to make x and y variables for values in ins list
						int x = Int16.Parse(ins[1]);
						int y = Int16.Parse(ins[2]);
						//Graphic g and x and y value are passed to Drawto command
						MainDraw.DrawTo(g, x, y);
					}
					catch (System.FormatException e)
					{
						//suitable output to user
						TextOutput.Text = "Parameters given have incorrect datatype";
						// if in syntax mode
						if (Globals.SyntaxMode == true)
						{
							// syntax is updated and error message stated
							Globals.SyntaxError = true;
							Globals.SysntaxErrorMessage = "Parameters given have incorrect datatype";
						}
					}
				}
				else
				{
					//suitable output to user
					TextOutput.Text = "Invalid number of Parameters given";
					// if in syntax mode
					if (Globals.SyntaxMode == true)
					{
						// syntax is updated and error message stated
						Globals.SyntaxError = true;
						Globals.SysntaxErrorMessage = "Invalid number of Parameters given";
					}
				}
			}
			else if (CommandValue.Contains("declarevar"))
			{
				//if CommandValue contains declarevar then Command_Variable is run
				Command_Variable(CommandValue);
			}
			else if (CommandValue.Contains("modifyvar"))
			{
				//if CommandValue contains modifyvar then Modifyvar is run
				ModifyVar(CommandValue);
			}
			else if (CommandValue.Contains("forward"))
			{
				//list ins made by splitting CommandValue
				String[] ins = CommandValue.Split(delimiterChars);
				//checks length of ins
				if (ins.Length == 2)
				{
					try
					{
						//string variable becomes value of position 1 in ins list
						String w = ins[1];
						//distance becomes int w
						int distance = Int16.Parse(w);
						//forward command is run and passed grapgic g and distance variable value
						MainDraw.Forward(g, distance);
					}
					catch (System.FormatException e)
					{
						//suitable output to user
						TextOutput.Text = "Invalid datatype given";
						if (Globals.SyntaxMode == true)
						{
							// syntax is updated and error message stated
							Globals.SyntaxError = true;
							Globals.SysntaxErrorMessage = "Invalid datatype given";
						}
					}
				}
				else
				{
					//suitable output to user
					TextOutput.Text = "Invalid Command";
					if (Globals.SyntaxMode == true)
					{
						// syntax is updated and error message stated
						Globals.SyntaxError = true;
						Globals.SysntaxErrorMessage = "Invalid number of Parameters given";
					}
				}
			}
			else
			{
				if (CommandValue.Contains("backwards"))
				{
					//list ins made by splitting CommandValue
					String[] ins = CommandValue.Split(delimiterChars);
					// length of ins checked
					if (ins.Length == 2)
					{
						try
						{
							//string variable becomes value of position 1 in ins list
							String w = ins[1];
							//distance becomes int w
							int distance = Int16.Parse(w);
							//make that value negative
							int back_distance = -distance;
							//Forward command is run and passed graphic g and back_distance
							MainDraw.Forward(g, back_distance);
						}
						catch (System.FormatException e)
						{
							//suitable output to user
							TextOutput.Text = "Invalid datatype given";
							if (Globals.SyntaxMode == true)
							{
								// syntax is updated and error message stated
								Globals.SyntaxError = true;
								Globals.SysntaxErrorMessage = "Invalid datatype given";
							}
						}
					}
					else
					{
						//suitable output to user
						TextOutput.Text = "Invalid number of Parameters given";
						if (Globals.SyntaxMode == true)
						{
							// syntax is updated and error message stated
							Globals.SyntaxError = true;
							Globals.SysntaxErrorMessage = "Invalid number of Parameters given";
						}
					}
				}
				else
				{
					//switch is used for all other basic 1 word commands
					switch (CommandValue)
					{
						case "turnleft": MainDraw.TurnLeft(); break;
						case "turnright": MainDraw.TurnRight(); break;
						case "penup": MainDraw.PenUp(); break;
						case "pendown": MainDraw.PenDown(); break;
						case "openscript": OpenScript(); break;
						case "savescript": SaveScript(); break;
						case "run": ScriptRun(); break;
						case "reset": MainDraw.Reset(); break;
						case "clear": MainDraw.Clear(g); break;
						case "endif": break;
						case "endloop": break;
						default: TextOutput.Text = ("Invalid Command"); Globals.SyntaxError = true; Globals.SysntaxErrorMessage = "Invalid command"; break;
					}
				}
			}
		}
		/// <summary>
		/// This method is used to create variables
		/// </summary>
		/// <param name="LineInput">This string contains the line used to declare the variable including it's name, dataype, and value</param>
		/// <example>
		/// The following are valid examples the method expects from lineinput
		/// <br></br>
		/// <code language="cs">
		/// declarevar 'varname' int 30
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// declarevar 'varname' string hello
		/// </code>
		/// <br></br>
		/// </example>
		public void Command_Variable(String LineInput)
		{
			//string variable becomes the same as LineInput
			String CommandValue = LineInput;
			//character lists are made
			char[] delimiterChars = { ' ' };
			char[] badChars = new char[] { '!', '?' };
			// CommandValue is made lowercase
			CommandValue = CommandValue.ToLower();
			//CommandValue is split into ins
			String[] ins = CommandValue.Split(delimiterChars);
			//new variables made
			int testlength = ins.Length;
			Boolean badcharCheck = false;
			String errormessage = "";
			try
			{
				// int variable amde to act as starting point for a loop
				int badcheckstart = 0;
				//character list is made from CommandValue
				Char[] insforchar = CommandValue.ToCharArray();
				//int value becomes length of insforchar list
				int badcharChecklength = badChars.Length;
				//while loop is started
				while (badcheckstart < badcharChecklength)
				{
					//checkes to see if insfo5char contains the current charcter being looked at
					if (insforchar.Contains(badChars[badcheckstart]))
					{
						//if so boolean value changes
						badcharCheck = true;
						//user created exception is thrown
						throw new VarNameException(badChars[badcheckstart]);
						break;

					}
					badcheckstart++;
				}
			}
			// catch catches the user made exception
			catch (VarNameException ex)
			{
				//boolean value is updated 
				badcharCheck = true;
				//errormesage string becomes the value of the message from the caught error
				errormessage = ex.Message;
			}
			//checks to see if badchar check is false
			if (badcharCheck == false)
			{
				//chedcks length of ins list
				if (ins.Length == 4)
				{
					//checks if int is present
					if (ins[2] == "int")
					{
						//variable becomes the expcted variable name
						String VariableName = ins[1];
						try
						{
							//variablevalue becomes expected value of the variable and made an int
							int VariableValue = Int16.Parse(ins[3]);
							//variable anema dn then variable value are added the the Variables arraylist
							Variables.Add(VariableName);
							Variables.Add(VariableValue);
							//relevant output to the user
							TextOutput.Text = ("Valid Int Variable " + VariableName + " Created");
						}
						catch (System.FormatException e)
						{
							//relevant output to the user
							TextOutput.Text = ("Invalid Value for Declared Data Type");
							// if in syntax mode
							if (Globals.SyntaxMode == true)
							{
								// syntax is updated and error message stated
								Globals.SyntaxError = true;
								Globals.SysntaxErrorMessage = "Invalid value for declared datatype";
							}
						}

					}
					else if (ins[2] == "string")
					{
						//String variables declared
						String VariableName = ins[1];
						String VariableValue = ins[3];
						//Variable name and vlaue added to Variables arraylist
						Variables.Add(VariableName);
						Variables.Add(VariableValue);
						//suitable output to user
						TextOutput.Text = ("Valid String Variable " + VariableName + " Created");
					}
					else
					{
						//relevant output to the user
						TextOutput.Text = ("Invalid Datatype");
						// if in syntax mode
						if (Globals.SyntaxMode == true)
						{
							// syntax is updated and error message stated
							Globals.SyntaxError = true;
							Globals.SysntaxErrorMessage = "Invalid datatype declared";
						}
					}

				}
				else
				{
					//relevant output to the user
					TextOutput.Text = ("Invalid Declaration");
					// if in syntax mode
					if (Globals.SyntaxMode == true)
					{
						// syntax is updated and error message stated
						Globals.SyntaxError = true;
						Globals.SysntaxErrorMessage = "Invalid number of Parameters given";
					}
				}
			}
			else
			{
				//relevant output to the user
				TextOutput.Text = (errormessage);
				// if in syntax mode
				if (Globals.SyntaxMode == true)
				{
					// syntax is updated and error message stated
					Globals.SyntaxError = true;
					Globals.SysntaxErrorMessage = errormessage;
				}
			}
		}
		/// <summary>
		/// This method is used to modify the value of an existing variable
		/// </summary>
		/// <param name="LineInput"></param>
		/// <example>
		/// The following are valid examples the method expects from lineinput
		/// <br></br>
		/// <code language="cs">
		/// modifyvar 'varname' int 30
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// modifyvar 'varname' string hello
		/// </code>
		/// <br></br>
		/// <code language = "cs" >
		/// modifyvar 'varname' int ++
		/// </code>
		/// <br></br>
		/// <code language="cs">
		/// modifyvar 'varname' string hello
		/// </code>
		/// <br></br>
		/// </example>
		public void ModifyVar(string LineInput)
		{
			//string variable becomes the same as LineInput
			String CommandValue = LineInput;
			//character list is made
			char[] delimiterChars = { ' ' };
			//CommandValue is made lowercase
			CommandValue = CommandValue.ToLower();
			//CommandValue is split into the list ins
			String[] ins = CommandValue.Split(delimiterChars);
			int testlength = ins.Length;
			//checks length of ins
			if (ins.Length == 4)
			{
				//checks to see if int is present
				if (ins[2] == "int")
				{
					//variable name is defined
					String VariableName = ins[1];
					try
					{
						//program checks for variable and it's value
						string Variablevalue = ins[3];
						int varnamepos = Variables.IndexOf(ins[1]);
						int varvaluepos = varnamepos + 1;
						var variablevalue = Variables[varvaluepos];
						String VariablevalueString = variablevalue.ToString();
						int Varcurrentvalue = Int16.Parse(VariablevalueString);
						//program checks if ++ is present
						if (ins[3].Contains("++"))
						{
							//variable value is increased by 1
							Varcurrentvalue = Varcurrentvalue + 1;
							//Variable's value is updated
							Variables[varvaluepos] = Varcurrentvalue;
							//relevant output to the user
							TextOutput.Text = ("Valid Int Variable " + VariableName + " value has increased by 1 to " + Varcurrentvalue);
						}
						//program checks to see if -- is present
						else if (ins[3].Equals("--"))
						{
							//variable value is decreased by 1
							Varcurrentvalue = Varcurrentvalue - 1;
							//Variable's value is updated
							Variables[varvaluepos] = Varcurrentvalue;
							//releavnt output to user
							TextOutput.Text = ("Valid Int Variable " + VariableName + " value has decreased by 1 to " + Varcurrentvalue);
						}
						else
						{
							//variablevalue is made into a integer
							int VariableValue = Int16.Parse(Variablevalue);
							//variable's value is updated
							Variables[varvaluepos] = VariableValue;
							//relevant output to the user
							TextOutput.Text = ("Valid Int Variable " + VariableName + " updated to value of " + VariableValue);
						}

					}
					catch (System.FormatException e)
					{
						//releavnt output to the user
						TextOutput.Text = ("Invalid Value for Declared Datatype " + ins[3]);
						// if in syntax mode
						if (Globals.SyntaxMode == true)
						{
							// syntax is updated and error message stated
							Globals.SyntaxError = true;
							Globals.SysntaxErrorMessage = "Invalid value for the declared datatype";
						}
					}

				}
				//checks to see if string is present
				else if (ins[2] == "string")
				{
					//program checks for variable and it's value
					String VariableName = ins[1];
					String VariableValue = ins[3];
					int varnamepos = Variables.IndexOf(ins[1]);
					int varvaluepos = varnamepos + 1;
					//Variable value is updated
					Variables[varvaluepos] = VariableValue;
					//relevant output to the user
					TextOutput.Text = ("Valid String Variable " + VariableName + " updated to value of " + VariableValue);
				}
				else
				{
					//relevant output to user
					TextOutput.Text = ("Invalid Datatype");
					// if in syntax mode
					if (Globals.SyntaxMode == true)
					{
						// syntax is updated and error message stated
						Globals.SyntaxError = true;
						Globals.SysntaxErrorMessage = "Invalid datatype given";
					}
				}

			}
			else
			{
				//relevant output to user
				TextOutput.Text = ("Invalid Declaration");
				// if in syntax mode
				if (Globals.SyntaxMode == true)
				{
					// syntax is updated and error message stated
					Globals.SyntaxError = true;
					Globals.SysntaxErrorMessage = "Invalid number of Parameters given";
				}
			}
		}
		
		private void RunButton_Click(object sender, EventArgs e)
		{
			//Commandline becomes value in CommandInput
			String Commandline = CommandInput.Text;
			//CommandParser is run and passed Commandline
			CommandParser(Commandline);
		}
	}
}
