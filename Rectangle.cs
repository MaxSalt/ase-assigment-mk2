﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace ASE_Assignment
{
    class Rectangle : Shape
    {
        int width;
        int height;
        public Rectangle() : base()
        {
            width = 100;
            height = 100;
        }

        public Rectangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {
            this.width = width;
            this.height = height;
        }

        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }

        public override void draw(Graphics g)
        {
            Pen p = new Pen(Globals.PenWritingColour);
            SolidBrush b = new SolidBrush(colour);
            if (Globals.shapeFill == true)
            {
                g.FillRectangle(b, x, y, width, height);
            }
            else if (Globals.shapeDraw == true)
            {
                g.DrawRectangle(p, x, y, width, height);
            }
            p.Dispose();
            b.Dispose();
        }


        public override double calcArea()
        {
            return width = height;
        }


        public override double calcPerimeter()
        {
            return 2 * width + 2 * height;
        }

    }
}
