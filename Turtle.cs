﻿using System;              //TURTLE.CS CLASS
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ASE_Assignment
{

	

	/// <summary>
	/// Main turtle class used for drawing lines and various other aspects concerning the picturebox
	/// </summary>
	public class Turtle
	{
		//new shapefactory is declared
		ShapeFactory Factory = new ShapeFactory();
		//Pen CurrentPen = new Pen(Color.Black, 2); //INSTANTIATE PEN OBJECT


		public Turtle()
		{
			Console.WriteLine("Turtle has been created"); //debug code, delete
		}
		/// <summary>
		/// Method is sued to clear picturebox
		/// </summary>
		/// <param name="g"></param>
		public void Clear(Graphics g) //EXAMPLE FUNCTION TO CLEAR GRAPHICS G
		{
			g.Clear(Color.White);
		}
		/// <summary>
		/// Method used to reset all the main values in the program
		/// </summary>
		public void Reset() 
		{
			Globals.xPos = Globals.DefaultxPos;
			Globals.yPos = Globals.DefaultyPos;
			Globals.currentLine = 0;
			Globals.ScriptLength = 0;
		}
		/// <summary>
		/// Method used to lower the pen
		/// </summary>
		public void PenDown()
		{
			Globals.penWriting = true;

		}
		/// <summary>
		/// Method used to delcare if objects are filled or not
		/// </summary>
		/// <param name="Decision"></param>
		public void FillObjects(Boolean Decision)
		{
			//global variable values change based on what is based to the method
			Globals.shapeFill = Decision;
			Globals.shapeDraw = !Decision;
			
		}
		/// <summary>
		/// Method used to raise pen
		/// </summary>
		public void PenUp()
		{
			//sets penwriting to false
			Globals.penWriting = false;
		}
		/// <summary>
		/// Method used to move the pen to a given
		/// </summary>
		/// <param name="x">int value for x axis value</param>
		/// <param name="y">int value for y axis value</param>
		public void MovePen(int x, int y)
		{
			//sets the global x and y values to that stated on method declaration
			Globals.xPos = x;
			Globals.yPos = y;
		}
		/// <summary>
		/// Method used to turn turtle right
		/// </summary>
		public void TurnRight()
		{
			Globals.direction = Globals.direction + 90;
			if (Globals.direction >= 360)
				Globals.direction = 0;
			return;
		}
		/// <summary>
		/// Method used to turn turtle left
		/// </summary>
		public void TurnLeft()
		{
			Globals.direction = Globals.direction - 90;
			if (Globals.direction < 0)
				Globals.direction = 270;
			return;
		}
		/// <summary>
		/// Command used to draw to a specific x and y position
		/// </summary>
		/// <param name="g">Name of the Grpahics element</param>
		/// <param name="x">int value for x axis value</param>
		/// <param name="y">int value for y axis value</param>
		public void DrawTo(Graphics g, int x, int y) 
		{
			Pen CurrentPen = new Pen(Globals.PenWritingColour, 2);
			if (Globals.penWriting == true)
			{
				g.DrawLine(CurrentPen, Globals.xPos, Globals.yPos, x, y);
			}
			CurrentPen.Dispose();
			Globals.xPos = x;
			Globals.yPos = y;
			return;
		}
		/// <summary>
		/// Method used to move turtle forward
		/// </summary>
		/// <param name="g"></param>
		/// <param name="distance"></param>
		public void Forward(Graphics g, int distance)
		{
			Pen CurrentPen = new Pen(Globals.PenWritingColour, 2);
			//Graphics g = image.getGraphics();
			int x = Globals.xPos;
			int y = Globals.yPos;
			//stored xPos and yPos are current location
			if (Globals.direction == 0) //robot facing up the screen, so forward subtracts y
			{
				y = Globals.yPos - distance;
			}
			else if (Globals.direction == 90) //robot facing right so forward add x
			{
				x = Globals.xPos + distance;
			}
			else if (Globals.direction == 180) //robot facing down the screen, so forwards adds to y
			{
				y = Globals.yPos + distance;
			}
			else if (Globals.direction == 270) //robot facing left, so forwards subtracts from x
			{
				x = Globals.xPos - distance;
			}
			if (Globals.penWriting == true)
			{
				g.DrawLine(CurrentPen, Globals.xPos, Globals.yPos, x, y);
			}
			CurrentPen.Dispose();
			Globals.xPos = x;
			Globals.yPos = y;
			return;
		}
	}
}